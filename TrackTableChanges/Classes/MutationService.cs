﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackTableChanges.Classes
{
    public interface IMutationService
    {
        /// <summary>
        /// Gets latest fard summary Async
        /// </summary>
        /// <returns></returns>
        Task<MutationSummary> GetTehsilMutationSummaryAsync(int mozaId, long tokenId);
    }

    public class MutationService : IMutationService
    {
        public IDBLayer db;

        public MutationService()
        {
            db = new DBLayer();
        }

        public async Task<MutationSummary> GetTehsilMutationSummaryAsync(int mozaId, long tokenId)
        {
            MutationSummary summary = new MutationSummary();
            try
            {
                int tehsilId = 0;
                string getTehsilQuery = "SELECT TehsilId FROM Moza WHERE MozaId = " + mozaId;
                string data = await db.ExecuteScalarQueryAsync(getTehsilQuery);
                if (data != null)
                {
                    tehsilId = Convert.ToInt32(data);
                }

                string query = "Select * from Vw_Dashboard_IntiqalMain WHERE TehsilId = " + tehsilId + " AND CAST(InsertDate AS DATE) = '" + DateTime.Now.Date + "'";
                var results = await db.ExecuteReaderQueryAsync(query);
                if (results != null)
                {
                    var mutationsCounts = from result in results
                                     group result[1] by result[2] into g
                                     select new { TehsilId = g.Key, Mutations = g.Count() };

                    foreach (var mutation in mutationsCounts)
                    {
                        summary.TehsilId = Convert.ToInt32(mutation.TehsilId);
                        summary.Mutations = mutation.Mutations;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return await Task<FardSummary>.Run(() => summary).ConfigureAwait(false);
        }
    }
}
