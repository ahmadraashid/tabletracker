﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackTableChanges.Classes
{
    public interface IUtilityService
    {
        int GetDistrictForTehsil(int tehsilId);
    }

    public class UtilityService : IUtilityService
    {
        private IDBLayer db;

        public UtilityService()
        {
            db = new DBLayer();
        }

        public int GetDistrictForTehsil(int tehsilId)
        {
            int districtId = 0;
            try
            {
                string query = "SELECT DistrictId from Tehsils WHERE TehsilId = " + tehsilId;
                districtId = Convert.ToInt32(db.ExecuteScalarQuery(query));
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return districtId;
        }
    }
}
