﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackTableChanges.Classes
{
    public interface IDocumentService
    {
        Task<DocumentSummary> GetDocumentsSummaryAsync(int tehsilId);
    }

    public class DocumentService : IDocumentService
    {
        private IDBLayer db;

        public DocumentService()
        {
            db = new DBLayer();
        }


        public async Task<DocumentSummary> GetDocumentsSummaryAsync(int tehsilId)
        {
            DocumentSummary summary = new DocumentSummary();
            try
            {
                string query = "Select TehsilId, DocumentTypeId from FilesReceiving WHERE TehsilId = " + tehsilId + " AND CAST(InsertDate AS DATE) = '" + DateTime.Now.Date + "'";
                var results = await db.ExecuteReaderQueryAsync(query);
                if (results != null)
                {
                    var documentCounts = from result in results
                                     group result by new
                                     {
                                         TehsilId = result[0],
                                         DocumentTypeId = result[1]

                                     } into g
                                     select new { TehsilId = g.Key.TehsilId, DocumentTypeId = g.Key.DocumentTypeId, Documents = g.Count() };

                    foreach (var document in documentCounts)
                    {
                        summary.TehsilId = Convert.ToInt32(document.TehsilId);
                        summary.DocumentTypeId = Convert.ToInt32(document.DocumentTypeId);
                        summary.Count = document.Documents;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return await Task<DocumentSummary>.Run(() => summary).ConfigureAwait(false);
        }
    }
}
