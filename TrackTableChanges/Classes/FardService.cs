﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace TrackTableChanges.Classes
{
    public interface IFardService
    {
        /// <summary>
        /// Gets latest fard summary
        /// </summary>
        /// <returns></returns>
        Task<FardSummary> GetTehsilFardSummary(int tehsilId, long tokenId);
    }

    public class FardService : IFardService
    {
        private IDBLayer db;

        public FardService()
        {
            db = new DBLayer();
        }

        public async Task<FardSummary> GetTehsilFardSummary(int tehsilId, long tokenId)
        {
            FardSummary summary = new FardSummary();
            try
            {
                string query = "Select * from VW_Dashboard_Tokens WHERE TehsilId = " + tehsilId + " AND CAST(InsertDate AS DATE) = '" + DateTime.Now.Date + "'";
                var results = await db.ExecuteReaderQueryAsync(query);
                if (results != null)
                {
                    var fardCounts = from result in results
                                     group result[0] by result[1] into g
                                     select new { TehsilId = g.Key, Fards = g.Count() };

                    foreach (var fard in fardCounts)
                    {
                        summary.TehsilId = Convert.ToInt32(fard.TehsilId);
                        summary.Fards = fard.Fards;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return await Task<FardSummary>.Run(() => summary).ConfigureAwait(false);
        }
    }
}
