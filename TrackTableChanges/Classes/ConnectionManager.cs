﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackTableChanges.Classes
    {

    public interface IconnectionManager
        {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        string GetConnectionString();
        }

    public class ConnectionManager : IconnectionManager
        {
        public string connectionString = "";

        public ConnectionManager()
            {
            this.connectionString = ConfigurationManager.ConnectionStrings["TTC"].ToString();
            }

        public string GetConnectionString()
            {
            return this.connectionString;
            }
        }
    }
