﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackTableChanges.Classes
{
    public interface ITokenService
    {
        /// <summary>
        /// Gets revenue amount for fards for the current date and tehsil id
        /// </summary>
        /// <param name="tehsilId"></param>
        /// <returns></returns>
        Task<decimal> GetFardRevenueAmount(long tokenId);

        /// <summary>
        /// Gets revenue amount for mutations for the current date and tehsil id
        /// </summary>
        /// <param name="tehsilId"></param>
        /// <returns></returns>
        Task<decimal> GetMutationRevenueAmount(long tokenId);
    }

    public class TokenService : ITokenService
    {
        public IDBLayer db;

        public TokenService()
        {
            db = new DBLayer();
        }

        public async Task<decimal> GetFardRevenueAmount(long tokenId)
        {
            decimal tokenAmount = 0;
            try
            {
                string getAmountQuery = "SELECT Amount, InsertDate FROM VW_Dashboard_Tokens" +
                                        " WHERE TokenId = " + tokenId + " AND CAST(InsertDate AS DATE) = '" + DateTime.Now.Date + "'";
                var results = await db.ExecuteReaderQueryAsync(getAmountQuery);
                if (results != null)
                {
                    foreach(var result in results)
                    {
                        tokenAmount = Convert.ToDecimal(result["Amount"]);
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return await Task<int>.Run(() => tokenAmount).ConfigureAwait(false);
        }

        public async Task<decimal> GetMutationRevenueAmount(long tokenId)
        {
            decimal tokenAmount = 0;
            try
            {
                string getAmountQuery = "SELECT MutationAmount as Amount, InsertDate FROM VW_Dashboard_IntiqalMain" +
                                        " WHERE TokenId = " + tokenId + " AND CAST(InsertDate AS DATE) = '" + DateTime.Now.Date + "'";
                var results = await db.ExecuteReaderQueryAsync(getAmountQuery);
                if (results != null)
                {
                    foreach (var result in results)
                    {
                        tokenAmount = Convert.ToDecimal(result["Amount"]);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return await Task<int>.Run(() => tokenAmount).ConfigureAwait(false);
        }
    }
}
