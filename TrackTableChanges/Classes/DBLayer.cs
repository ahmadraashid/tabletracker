﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackTableChanges.Classes
{
    public interface IDBLayer
    {
        /// <summary>
        /// returns number of rows for the provided query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<DataRow> ExecuteReaderQuery(string query);

        /// <summary>
        /// returns number of rows for the provided query Async
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<IEnumerable<DataRow>> ExecuteReaderQueryAsync(string query);

        /// <summary>
        /// returns first column for the provided query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        string ExecuteScalarQuery(string query);

        /// <summary>
        /// returns first column for the provided query Async
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<string> ExecuteScalarQueryAsync(string query);
    }

    public class DBLayer : IDBLayer
    {
        private string connectionString;
        private IconnectionManager connManager;

        public DBLayer()
        {
            connManager = new ConnectionManager();
            this.connectionString = connManager.GetConnectionString();
        }

        public IEnumerable<DataRow> ExecuteReaderQuery(string query)
        {
            using (var connection = new SqlConnection(this.connectionString))
            {
                IEnumerable<DataRow> rows = null;
                using (var command = new SqlCommand())
                {
                    command.CommandText = query;
                    command.Connection = connection;
                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        DataTable dt = new DataTable();
                        dt.Load(reader);
                        rows = dt.AsEnumerable();
                    }
                }
                return rows;
            }
        }

        public async Task<IEnumerable<DataRow>> ExecuteReaderQueryAsync(string query)
        {
            using (var connection = new SqlConnection(this.connectionString))
            {
                IEnumerable<DataRow> rows = null;
                using (var command = new SqlCommand())
                {
                    command.CommandText = query;
                    command.Connection = connection;
                    command.Connection.Open();

                    SqlDataReader reader = await command.ExecuteReaderAsync();
                    if (reader.HasRows)
                    {
                        DataTable dt = new DataTable();
                        dt.Load(reader);
                        rows = dt.AsEnumerable();
                    }
                }
                return await Task.Run<IEnumerable<DataRow>>(() => rows).ConfigureAwait(false);
            }
        }

        public string ExecuteScalarQuery(string query)
        {
            using (var connection = new SqlConnection(this.connectionString))
            {
                string returnValue = "";
                using (var command = new SqlCommand())
                {
                    command.CommandText = query;
                    command.Connection = connection;
                    command.Connection.Open();

                    var firstColumn = command.ExecuteScalar();
                    if (firstColumn != null)
                        returnValue = firstColumn.ToString();
                }
                return returnValue;
            }
        }

        public async Task<string> ExecuteScalarQueryAsync(string query)
        {
            using (var connection = new SqlConnection(this.connectionString))
            {
                string returnValue = "";
                using (var command = new SqlCommand())
                {
                    command.CommandText = query;
                    command.Connection = connection;
                    command.Connection.Open();

                    var firstColumn = await command.ExecuteScalarAsync();
                    if (firstColumn != null)
                        returnValue = firstColumn.ToString();
                }
                return await Task.Run<string>(() => returnValue).ConfigureAwait(false);
            }
        }


    }
}
