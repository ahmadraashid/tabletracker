﻿using System;
using Newtonsoft.Json;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace TrackTableChanges.Classes
{
    public interface IAPIClient
    {
        /// <summary>
        /// Posts latest summary of fard for a particular tehsil under a district
        /// </summary>
        /// <param name="summary"></param>
        /// <returns></returns>
        Task<APIResponse> PostFardSummary(FardSummary summary);

        /// <summary>
        /// Posts latest summary of mutation for a particular tehsil under a district
        /// </summary>
        /// <param name="summary"></param>
        /// <returns></returns>
        Task<APIResponse> PostMutationSummary(MutationSummary summary);

        /// <summary>
        /// Posts latest summary on document statistics
        /// </summary>
        /// <param name="summary"></param>
        /// <returns></returns>
        Task<APIResponse> PostDocumentSummary(DocumentSummary summary);
    }

    public class APIClient : IAPIClient
    {
        APIResponse response;
        string baseUrl;

        public APIClient()
        {
            baseUrl = ConfigurationManager.AppSettings["ApiBaseUrl"].ToString();
        }

        public async Task<APIResponse> PostFardSummary(FardSummary summary)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    this.MakeClientSettings(client);
                    response = new APIResponse() { Success = false };
                    var responseMessage = await client.PostAsJsonAsync(baseUrl + "/Fard", summary);
                    var message = responseMessage.Content.ReadAsStringAsync().Result;
                    response.Message = responseMessage.Content.ReadAsStringAsync().Result;

                    if (responseMessage.IsSuccessStatusCode)
                    {
                        response.Success = true;
                    }
                    else
                    {
                        response.Success = false;
                    }
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = ex.Message;
                }

                return response;
            }
        }

        public async Task<APIResponse> PostMutationSummary(MutationSummary summary)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    this.MakeClientSettings(client);
                    response = new APIResponse() { Success = false };
                    var responseMessage = await client.PostAsJsonAsync(baseUrl + "/Mutation", summary);
                    var message = responseMessage.Content.ReadAsStringAsync().Result;
                    response.Message = responseMessage.Content.ReadAsStringAsync().Result;

                    if (responseMessage.IsSuccessStatusCode)
                    {
                        response.Success = true;
                    }
                    else
                    {
                        response.Success = false;
                    }
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = ex.Message;
                }

                return response;
            }
        }

        public async Task<APIResponse> PostDocumentSummary(DocumentSummary summary)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    this.MakeClientSettings(client);
                    response = new APIResponse() { Success = false };
                    var responseMessage = await client.PostAsJsonAsync(baseUrl + "/Document", summary);
                    var message = responseMessage.Content.ReadAsStringAsync().Result;
                    response.Message = responseMessage.Content.ReadAsStringAsync().Result;

                    if (responseMessage.IsSuccessStatusCode)
                    {
                        response.Success = true;
                    }
                    else
                    {
                        response.Success = false;
                    }
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = ex.Message;
                }

                return response;
            }
        }

        //Do client settings for every request
        private void MakeClientSettings(HttpClient client)
        {
            client.BaseAddress = new Uri(this.baseUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

    }
}
