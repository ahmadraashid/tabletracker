﻿using System;

namespace TrackTableChanges.Classes
{
    public class APIResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public int ReturnedId { get; set; }
    }

    public class FardSummary
    {
        public int TehsilId { get; set; }
        public int Fards { get; set; }
        public decimal Revenue { get; set; }
    }

    public class DocumentSummary
    {
        public enum DocumentStatus
        {
            Received = 1,
            InProcess = 2,
            Pending = 3,
            Completed = 4
        }

        public int FileId { get; set; }
        public int DistrictId { get; set; }
        public int TehsilId { get; set; }
        public int DocumentTypeId { get; set; }
        public DocumentStatus Status { get; set; }
        public int Count { get; set; }
    }

    public class MutationSummary
    {
        public int TehsilId { get; set; }
        public int Mutations { get; set; }
        public decimal Revenue { get; set; }
    }

    public class SDC_FardPersonsGroup
    {
        public long FPGId { get; set; }
        public int Total_Quantity { get; set; }
        public long TokenId { get; set; }
        public int TehsilId { get; set; }
        public DateTime InsertDate { get; set; }
    }

    public class Intiqal_Main
    {
        public int IntiqalId { get; set; }
        public int MozaId { get; set; }
        public int IntiqalTypeId { get; set; }
        public DateTime IntiqalAndrajDate { get; set; }
        public long TokenId { get; set; }
    }

    public class Fards
    {
        public string District { get; set; }
        public decimal Points { get; set; }
    }

    public class FilesReceiving
    {
        public int ReceivingId { get; set; }
        public int DistrictId { get; set; }
        public int TehsilId { get; set; }
        public int MozaId { get; set; }
        public int DocumentTypeId { get; set; }
        public int ActivityStatus { get; set; }
    }

}
