﻿using System;
using TableDependency.Enums;
using TableDependency.EventArgs;
using TableDependency.SqlClient;
using TrackTableChanges.Classes;

namespace TrackTableChanges
{
    class Program
    {

        static void Main(string[] args)
        {
            ConnectionManager connectionManager = new ConnectionManager();
            string connectionString = connectionManager.GetConnectionString();
            try
            {
                //Listener for Fard Summary
                var fardSummary = new SqlTableDependency<SDC_FardPersonsGroup>(connectionString, "SDC_FardPersonsGroup");
                fardSummary.OnChanged += FardSummaryChanged;
                fardSummary.OnError += OnError;
                fardSummary.Start();

                //Listener for Mutation Summary
                var mutationSummary = new SqlTableDependency<Intiqal_Main>(connectionString, "Intiqal_Main");
                mutationSummary.OnChanged += MutationSummaryChanged;
                mutationSummary.OnError += OnError;
                mutationSummary.Start();

                //Listener for Document Summary
                var documentSummary = new SqlTableDependency<FilesReceiving>(connectionString, "FilesReceiving");
                documentSummary.OnChanged += DocumentSummaryChanged;
                documentSummary.OnError += OnError;
                documentSummary.Start();


                Console.WriteLine(@"Waiting for receiving notifications...");
                Console.WriteLine(@"Press a key to stop");
                Console.ReadKey();

                fardSummary.Stop();
                mutationSummary.Stop();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
        }


        static void OnError(object sender, ErrorEventArgs e)
        {
            throw e.Error;
        }

        static void FardSummaryChanged(object sender, RecordChangedEventArgs<SDC_FardPersonsGroup> e)
        {
            Console.WriteLine(Environment.NewLine);
            if (e.ChangeType == ChangeType.Insert)
            {
                var changedEntity = e.Entity;
                int tehsilId = changedEntity.TehsilId;
                long tokenId = changedEntity.TokenId;
                UpdateFardSummary(tehsilId, tokenId);
            }
        }

        static void MutationSummaryChanged(object sender, RecordChangedEventArgs<Intiqal_Main> e)
        {
            Console.WriteLine(Environment.NewLine);
            if (e.ChangeType == ChangeType.Insert)
            {
                var changedEntity = e.Entity;
                int mozaId = changedEntity.MozaId;
                long tokenId = changedEntity.TokenId;
                UpdateMutationSummary(mozaId, tokenId);
            }
        }

        static void DocumentSummaryChanged(object sender, RecordChangedEventArgs<FilesReceiving> e)
        {
            Console.WriteLine(Environment.NewLine);
            if (e.ChangeType == ChangeType.Insert || e.ChangeType == ChangeType.Update)
            {
                var changedEntity = e.Entity;
                int tehsilId = changedEntity.TehsilId;
                int districtId = changedEntity.DistrictId;
                int fileId = changedEntity.ReceivingId;
                int status = changedEntity.ActivityStatus;
                UpdateDocumentSummary(tehsilId, fileId, status);
            }
        }

        private static async void UpdateFardSummary(int tehsilId, long tokenId)
        {
            IFardService fardService = new FardService();
            ITokenService tokenService = new TokenService();
            IAPIClient apiClient = new APIClient();

            try
            {
                var summaryData = await fardService.GetTehsilFardSummary(tehsilId, tokenId);
                var revenueAmount = await tokenService.GetFardRevenueAmount(tokenId);

                if (summaryData != null)
                {
                    summaryData.Revenue = revenueAmount;
                    var response = await apiClient.PostFardSummary(summaryData);
                    if (response.Success)
                    {
                        Console.WriteLine("Fard Summary Updated at: " + DateTime.Now.ToLongTimeString());
                    }
                }
                else
                {
                    Console.WriteLine("Invocation made for empty data");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async void UpdateMutationSummary(int mozaId, long tokenId)
        {
            IMutationService mutationService = new MutationService();
            ITokenService tokenService = new TokenService();
            IAPIClient apiClient = new APIClient();

            try
            {
                var summaryData = await mutationService.GetTehsilMutationSummaryAsync(mozaId, tokenId);
                if (summaryData != null)
                {
                    summaryData.Revenue = await tokenService.GetMutationRevenueAmount(tokenId);
                    var response = await apiClient.PostMutationSummary(summaryData);
                    if (response.Success)
                    {
                        Console.WriteLine("Mutation Summary Updated at: " + DateTime.Now.ToLongTimeString());
                    }
                }
                else
                {
                    Console.WriteLine("Invocation made for empty data");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async void UpdateDocumentSummary(int tehsilId, int fileId, int status)
        {
            IDocumentService documentService = new DocumentService();
            IUtilityService utilityService = new UtilityService();
            IAPIClient apiClient = new APIClient();

            try
            {
                var summaryData = await documentService.GetDocumentsSummaryAsync(tehsilId);
                if (summaryData != null)
                {
                    int districtId = utilityService.GetDistrictForTehsil(tehsilId); 
                    if (districtId != 0)
                    {
                        summaryData.DistrictId = districtId;
                        summaryData.FileId = fileId;
                        summaryData.Status = (DocumentSummary.DocumentStatus)status;
                        var response = await apiClient.PostDocumentSummary(summaryData);
                        if (response.Success)
                        {
                            Console.WriteLine("Document Summary Updated at: " + DateTime.Now.ToLongTimeString());
                        }
                    }
                    else
                    {
                        Console.WriteLine("The provided district id not found");
                    }
                    
                }
                else
                {
                    Console.WriteLine("Invocation made for empty data in Document Summary");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


    }
}
